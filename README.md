# vlc-bridge-levelnews

watch LevelNews live stream in VLC

### using

`$ docker run -d -e 'LEVELNEWS_USER=user@email.com' -e 'LEVELNEWS_PASS=secret' -p 7777:7777 --name vlc-bridge-levelnews registry.gitlab.com/miibeez/vlc-bridge-levelnews`

`$ vlc http://localhost:7777/levelnews/playlist.m3u`

### require

subscription account
