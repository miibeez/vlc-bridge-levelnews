import os
import sys
from threading import Lock
import requests
import json
import secrets
import time

if os.environ.get("LEVELNEWS_USER") is None or os.environ.get("LEVELNEWS_PASS") is None:
    sys.stderr.write("LEVELNEWS_USER and LEVELNEWS_PASS need set\n")
    sys.exit(1)

mapping = {
    8: "c_span",
    9: "c_span2",
    13: "c_span_3",
    4: "bloomberg_tv_",
    5: "bloomberg_quicktake",
    7 : "news_nation",
    10: "law___crime",
    14: "nhk_world_japan",
    6: "france_24",
    12: "euro_new",
}

class Client:
    def __init__(self):
        self.user = os.environ["LEVELNEWS_USER"]
        self.passwd = os.environ["LEVELNEWS_PASS"]

        self.device = None
        self.loggedIn = False
        self.sessionID = ""
        self.sessionAt = 0
        self.mutex = Lock()

        self.session = requests.Session()
        self.session.headers.update({
            'User-Agent': 'okhttp/3.12.10'
        })
        self.load_device()

    def channels(self):
        with self.mutex:
            resp, error = self.api("v1/tvguide/channels?skip_tabs=1")
            if error:
                return None, error

            stations = []
            for ch in resp["response"]["data"]:
                if ch["target"]["pageAttributes"].get("isLive") == "true":
                    ch_id = ch["id"]
                    if mapping.get(ch_id) is None:
                        print(f"missing mapping for channel {ch_id}")
                        continue
                    station_id = mapping[ch_id]
                    station_name = ch["display"]["title"].strip()
                    station = {"id": station_id, "name": station_name}
                    stations.append(station)
            return stations, None

    def watch(self, id):
        with self.mutex:
            token, error = self.token()
            if error:
                return None, error

            if not self.loggedIn:
                error = self.login()
                if error:
                    return None, error

            path = "channel/live/" + id
            resp, error = self.api("v1/page/stream?path="+path)
            if error:
                return None, error

            if not resp["status"]:
                message = resp["error"]["message"]
                code = resp["error"]["code"]
                return None, f"Stream failure: {message} ({code})"

            for s in resp["response"]["streams"]:
                url = s["url"]
                return url, None

            return None, "missing stream"

    def load_device(self):
        with self.mutex:
            try:
                with open("levelnews-device.json", "r") as f:
                    self.device = json.load(f)
            except FileNotFoundError:
                self.device = secrets.token_hex(8)
                with open("levelnews-device.json", "w") as f:
                    json.dump(self.device, f)

    def token(self):
        if self.sessionID != "" and (time.time() - self.sessionAt) < 4 * 60 * 60:
            return self.sessionID, None
        self.loggedIn = False

        params = {
            "box_id": self.device,
            "tenant_code": "levelnews",
            "product": "levelnews",
            "device_id": "43",
            "device_sub_type": "AFTT,12,8.3",
            "display_lang_code": "eng",
            "timezone": "UTC",
        }
        response = self.session.get("https://levelnews-api.revlet.net/service/api/v1/get/token", params=params).json()

        if not response["status"]:
            error_details = response["error"]["details"]
            error_detail = response["error"]["detail"]
            return None, f"Token failure: {error_details} ({error_detail})"

        self.sessionID = response["response"]["sessionId"]
        self.sessionAt = time.time()
        return response["response"]["sessionId"], None

    def api(self, cmd, data=None):
        token, error = self.token()
        if error:
            return None, error

        headers = {
            "box-id": self.device,
            "session-id": token,
            "tenant-code": "levelnews",
        }
        url = f"https://levelnews-api.revlet.net/service/api/{cmd}"
        if data:
            response = self.session.post(url, json=data, headers=headers)
        else:
            response = self.session.get(url, headers=headers)
        if response.status_code != 200:
            return None, f"HTTP failure {response.status_code}: {response.text}"
        return response.json(), None

    def login(self):
        resp, error = self.api("auth/signin", {
            "login_id": self.user,
            "login_key": self.passwd,
            "login_mode": 1,
            "manufacturer": "AFTT",
            "os_version": "12",
            "app_version": "8.3",
        })
        if error:
            return error

        if not resp["status"]:
            message = resp["error"]["message"]
            code = resp["error"]["code"]
            return f"Login failure: {message} ({code})"
        self.loggedIn = True
        return None
